from django.apps import AppConfig


class AdminzConfig(AppConfig):
    name = 'adminz'
