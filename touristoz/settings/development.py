import os
from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.getenv('DEBUG')

ALLOWED_HOSTS = ['127.0.0.1', 'django.izmanish.online']

INSTALLED_APPS += (
        'adminz',
        'authz',
        'rest_framework',
        'rest_registration',
    )

DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': os.getenv('DB_NAME'),
            'USER': os.getenv('DB_USER'),
            'PASSWORD': os.getenv('DB_PASSWORD'),
            'PORT': '',
            'HOST': os.getenv('DB_HOST'),
        }
    }

REST_REGISTRATION = {
    'REGISTER_VERIFICATION_ENABLED': True,
    'REGISTER_VERIFICATION_URL': 'https://frontend-host/verify/',
    'RESET_PASSWORD_VERIFICATION_URL': 'https://frontend-host/reset-password/',
    'REGISTER_EMAIL_VERIFICATION_URL': 'https://frontend-host/verify-email/',

    'REGISTER_VERIFICATION_ONE_TIME_USE': True,
    'VERIFICATION_FROM_EMAIL': 'no-reply@example.com',
    'USER_LOGIN_FIELDS': 'username',
    'SEND_RESET_PASSWORD_LINK_SERIALIZER_USE_EMAIL': True,
    # 'VERIFICATION_SIGNER_USER_FIELD': 'username',
    'USER_VERIFICATION_ID_FIELD': 'username',
    # 'REGISTER_VERIFICATION_EMAIL_TEMPLATES': {
    #     'subject':  'account/email_subject.txt',
    #     'html_body':  'account/email_body.html',
    # }
}

# Email backend settings for Django
EMAIL_BACKEND = os.getenv('EMAIL_BACKEND')
EMAIL_HOST = os.getenv('EMAIL_HOST')
EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD')
EMAIL_PORT = os.getenv('EMAIL_PORT')
EMAIL_USE_TLS = True
