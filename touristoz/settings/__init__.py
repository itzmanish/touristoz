import os
from .base import *


if os.getenv('env') == 'development':
    from .development import *
else:
    from .production import *
